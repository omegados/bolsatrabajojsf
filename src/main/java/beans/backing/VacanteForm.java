package beans.backing;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIInput;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import beans.helper.DistritoHelper;
import beans.model.Candidato;

// Modificamos el Manage bean de Candidato inyecctándolo en este Manage bean

@Named
@RequestScoped
public class VacanteForm {
	@Inject
	private Candidato candidato;
	private boolean comentarioEnviado;
	
	@Inject
	private DistritoHelper distritoHelper;


	Logger log = LogManager.getRootLogger();
	
	public VacanteForm() {
		log.info("Creando el objeto VacanteForm");
		
	}
	
	public void setCandidato(Candidato candidato) {
		this.candidato = candidato;
	}
	
	public String enviar() {
		if (this.candidato.getNombre().equals("Juan")) {
			if (this.candidato.getApellido().equals("Perez")) {
				String msg = "Gracias, pero Juan Pérez ya trabaja con nosotros";
				FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, msg);
				FacesContext facesContext = FacesContext.getCurrentInstance();
				String componentId = null; // Para indicar que no vamos a asociar el mensaje a un componente especifico, así que será global.
				facesContext.addMessage(componentId,  facesMessage);
				return "index";  // index.xhtml
			}
			log.info("Entrando al caso de éxito");
			return "exito";   // redireccionamos a "exito.xhtml" Automáticamente busca una página xhtml
		}
		else {
			log.info("Entrando al caso de fallo");
			return "fallo";
		}
	}
	
	
	// Este Listener se utilizaba para insertar los componentes Select de forma estática en el index.xhtml
	public void codigoPostalListener2(ValueChangeEvent valueChangeEvent) {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		UIViewRoot uiViewRoot = facesContext.getViewRoot();   // Con este objeto podemos acceder a cualquier elemento en nuestra página JSF
		String nuevoCodigoPostal = (String) valueChangeEvent.getNewValue();
		if ("18008".equals(nuevoCodigoPostal)) {
//			UIInput distritoInputText = (UIInput) uiViewRoot.findComponent("vacanteForm:distrito");
//			String nuevoDistrito = "Zaidín";                      
//			distritoInputText.setValue(nuevoDistrito);            
//			distritoInputText.setSubmittedValue(nuevoDistrito);  
			
			UIInput distritoIdInputText = (UIInput) uiViewRoot.findComponent("vacanteForm:distritoId");
			String distritoId = "1";
			distritoIdInputText.setValue(distritoId);            
			distritoIdInputText.setSubmittedValue(distritoId); 
			
			UIInput ciudadInputText = (UIInput) uiViewRoot.findComponent("vacanteForm:ciudad");
			String nuevaCiudad = "Granada";
			ciudadInputText.setValue(nuevaCiudad);
			ciudadInputText.setSubmittedValue(nuevaCiudad);
			                                                      
			                                                      
		}
	}
	
	// Listener para rellenar el componente Select dinámicamente con la clase Distrito y DistritoHelper
	public void codigoPostalListener(ValueChangeEvent valueChangeEvent) {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		UIViewRoot uiViewRoot = facesContext.getViewRoot();   // Con este objeto podemos acceder a cualquier elemento en nuestra página JSF
		int nuevoCodigoPostal = ((Long) valueChangeEvent.getNewValue()).intValue();
		//int nuevoCodigoPostal = 18008;

			
		UIInput distritoIdInputText = (UIInput) uiViewRoot.findComponent("vacanteForm:distritoId");
		int distritoId = this.distritoHelper.getDistritoIdPorCP(nuevoCodigoPostal);
		distritoIdInputText.setValue(String.valueOf(distritoId));            
		distritoIdInputText.setSubmittedValue(String.valueOf(distritoId)); 
		
		UIInput ciudadInputText = (UIInput) uiViewRoot.findComponent("vacanteForm:ciudad");
		String nuevaCiudad = "Granada";
		ciudadInputText.setValue(nuevaCiudad);
		ciudadInputText.setSubmittedValue(nuevaCiudad);
			                                                      			                                                     
	}
	
	// Función para ocultar o mostrar el comentario. Por eso se cambia la variable bool cada vez que se ejecuta la función
	public void ocultarComentario(ActionEvent actionEvent) {
		this.comentarioEnviado = !this.comentarioEnviado;
	}
	
	public boolean isComentarioEnviado() {
		return comentarioEnviado;
	}

	public void setComentarioEnviado(boolean comentarioEnviado) {
		this.comentarioEnviado = comentarioEnviado;
	}
	
	public DistritoHelper getDistritoHelper() {
		return distritoHelper;
	}

	public void setDistritoHelper(DistritoHelper distritoHelper) {
		this.distritoHelper = distritoHelper;
	}

}
