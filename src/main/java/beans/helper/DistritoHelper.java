package beans.helper;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.faces.model.SelectItem;
import javax.inject.Named;
import beans.model.Distrito;

@RequestScoped
@Named
public class DistritoHelper {
	
	public List<Distrito> getDistritos(){
		List<Distrito> distritos = new ArrayList<>();
		
		int distritoId = 1;
		
		Distrito distrito = new Distrito(distritoId++, "Zaidín", 18008);
		distritos.add(distrito);
		
		distrito = new Distrito(distritoId++, "Centro", 18001);
		distritos.add(distrito);
		
		distrito = new Distrito(distritoId++, "Chana", 18006);
		distritos.add(distrito);
		
		return distritos;
	}
	
	public int getDistritoIdPorNombre(String nombreDistrito) {
		int distritoId = 0;
		List<Distrito> distritos = this.getDistritos();
		
		for (Distrito distrito: distritos) {
			if (distrito.getNombreDistrito().equals(nombreDistrito)) {
				distritoId = distrito.getDistritoId();
				break;
			}
		}
		return distritoId;
	}
	
	public int getDistritoIdPorCP(int codigoPostal ) {
		int distritoId = 0;
		List<Distrito> distritos = this.getDistritos();
		
		for (Distrito distrito: distritos) {
			if (distrito.getCodigoPostal() == codigoPostal) {
				distritoId = distrito.getDistritoId();
				break;
			}
		}
		return distritoId;
	}

	public List<SelectItem> getSelectItems(){
		List<SelectItem> selectItems = new ArrayList<>();
		List<Distrito> distritos = this.getDistritos();
		
		for (Distrito distrito: distritos) {
			SelectItem selectItem = new SelectItem(distrito.getDistritoId(), distrito.getNombreDistrito());
			selectItems.add(selectItem);			
		}
		return selectItems;
	}
	
}
