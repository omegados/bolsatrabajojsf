package beans.model;

public class Distrito {
	private int distritoId;
	private String nombreDistrito;
	private int codigoPostal;
	
	
	public Distrito() {
		super();
	}
	public Distrito(int distritoId, String nombreDistrito, int codigoPostal) {
		super();
		this.distritoId = distritoId;
		this.nombreDistrito = nombreDistrito;
		this.codigoPostal = codigoPostal;
	}
	public int getDistritoId() {
		return distritoId;
	}
	public void setDistritoId(int distritoId) {
		this.distritoId = distritoId;
	}
	public String getNombreDistrito() {
		return nombreDistrito;
	}
	public void setNombreDistrito(String nombreDistrito) {
		this.nombreDistrito = nombreDistrito;
	}
	public int getCodigoPostal() {
		return codigoPostal;
	}
	public void setCodigoPostal(int codigoPostal) {
		this.codigoPostal = codigoPostal;
	}
	
	

}
